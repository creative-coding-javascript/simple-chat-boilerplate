# Simple Chat Boilerplate

A simple and customizable chat boilerplate using HTML, CSS, and JavaScript. This project includes a basic chat interface with a chatbot that responds with random messages.

## Table of Contents

- [Overview](#overview)
- [Getting Started](#getting-started)
- [Usage](#usage)
- [Customization](#customization)
- [Contributing](#contributing)
- [License](#license)

## Overview

This boilerplate provides a starting point for creating a simple chat interface with a chatbot. It includes HTML and CSS for the chat UI, and JavaScript for handling user input and chatbot responses. The chatbot responds with random messages from a predefined list.

## Getting Started

To run this project, simply serve the `public/` folder with any web server.

For example you may install and run `http-server`:

```bash
npm install -g http-server
http-server ./public/
```

## Usage

The chat interface consists of a header, a message display area, an input field for user messages, and a send button. Users can type messages in the input field and press Enter or click the send button to send the message. The chatbot will respond with a random message after a delay.

## Customization

You can easily customize the chat interface and chatbot behavior by modifying the HTML, CSS, and JavaScript files. Here are some ideas for customization:

- Change the colors, fonts, and layout of the chat interface by editing the CSS in `style.css`.
- Add more responses to the chatbot by modifying the `randomAnswers` array in `worker.js`.
- Implement more advanced chatbot logic by editing the `worker.js` file.
- Integrate with external APIs or databases to fetch dynamic responses or store user conversations.

## Contributing

Contributions are welcome! Feel free to fork this repository and make pull requests to suggest improvements or add new features. Please ensure that your contributions follow the project guidelines and adhere to the license terms.

## License

This project is licensed under the GPL-v3 License.