document.addEventListener('DOMContentLoaded', () => {

    const chatBody = document.getElementById('chat-body');
    const chatInput = document.getElementById('chat-input');
    const sendButton = document.getElementById('send-button');

    sendButton.addEventListener('click', () => {
      const message = chatInput.value;
      if (message) {
        sendMessage(message)
      }
    });

    chatInput.addEventListener('keydown', (event) => {
      if (event.key === 'Enter' && !event.shiftKey) {
        const message = chatInput.value;
        if (message) {
          sendMessage(message)
        }
      }
    });
    const chatbot = new Worker('worker.js', {
      type: "module"
    })
    var lastMessage = null;
    chatbot.addEventListener('message', (event)=> {
        const {action, content} = event.data

        if (action == 'result') {
            lastMessage.innerHTML = content;
            lastMessage = null;
        } else if (action == 'update') {
            if (!lastMessage) {
                lastMessage = appendMessage('assistant', content);
            }
            lastMessage.innerHTML = content;
            chatBody.scrollTop = chatBody.scrollHeight;
        }
      
    });

    function sendMessage(message) {
      message = message.trim();
      appendMessage('me', message);
      chatbot.postMessage(message);
      chatInput.value = '';
    }

    function appendMessage(type, message) {
      const messageContainer = document.createElement('div');
      messageContainer.classList.add('chat-message');
      messageContainer.classList.add(type);
      messageContainer.innerHTML = message;
      chatBody.appendChild(messageContainer);
      return messageContainer;
    }
    
    // function receiveMessage(message) {
      
    // }


})