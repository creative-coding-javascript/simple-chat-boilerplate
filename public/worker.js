const randomAnswers = [
  'Ciao!',
  'How are you?',
  'Nice to hear from you!',
  'I\'m glad to help!',
  'See you!',
  'Hola!',
  '¿Cómo estás?',
  'Es genial recibir un mensaje!',
  'Encantado de ayudarte!',
  'Hasta luego!',
  'Bonjour!',
  'Comment ça va?',
  'C\'est agréable de recevoir un message!',
  'Je suis heureux de vous aider!',
  'À bientôt!',
  'Hallo!',
  'Wie geht es dir?',
  'Schön, von dir zu hören!',
  'Ich freue mich, helfen zu können!',
  'Bis bald!',
];

function getRandomPhrase() {
  const randomAnswerIndex = Math.floor(Math.random() * randomAnswers.length);
  return randomAnswers[randomAnswerIndex];
}


addEventListener('message', (event) => {


  const chunks = [];

  for (let index = 0; index < Math.floor(Math.random() * 24); index++) {
    chunks.push(getRandomPhrase());
  }

  var timeOffset = 2000;

  postMessage({
    action: 'update',
    content: "🐾"
  });


  var result = '';
  for (let chunk of chunks) {

    const delay = Math.random() * 200;

    timeOffset += delay;

    setTimeout(() => {
      result += " " + chunk;
      postMessage({
        action: 'update',
        content: result + "🦊"
      });

    }, timeOffset);
  }

  setTimeout(() => {
    postMessage({
      action: 'result',
      content: result
    });

  }, timeOffset);

});